import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import { Typography, Paper, Grid, LinearProgress } from '@material-ui/core';
import Styles from './styles'

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            skils: {
            }
        }
    }

    componentDidMount = () => {
        this.setState({
            skils: this.props.skils
        }, () => this.buildFisrtSkills())
    }

    componentWillReceiveProps = ({ skils }) => {
        this.setState({
            skils
        }, () => this.buildFisrtSkills())
    }

    buildFisrtSkills = () => {
        const { classes } = this.props
        let main = this.state.skils.map((e) => {
            return (
                <Grid item sm={6} lg={6} md={6} >
                    <Grid container spacing={24}>
                        <Grid item sm={12} lg={12} md={12} >
                            <Typography variant="body2" gutterBottom>
                                {e.name}
                            </Typography>
                            <LinearProgress variant="determinate" color="primary" value={0} />
                        </Grid>
                    </Grid>
                </Grid>
            )
        })
        this.setState({ main },()=>this.buildSkills())
    }

    buildSkills = () => {
        const { classes } = this.props
        let main = this.state.skils.map((e) => {
            return (
                <Grid item sm={6} lg={6} md={6} >
                    <Grid container spacing={24}>
                        <Grid item sm={12} lg={12} md={12} >
                            <Typography variant="caption" gutterBottom>
                                {`${e.name} ${e.percentage}%`}
                            </Typography>
                            <LinearProgress variant="determinate" color="primary" value={e.percentage} />
                        </Grid>
                    </Grid>
                </Grid>
            )
        })
        this.setState({ main })
    }

    render() {
        const { classes } = this.props
        return (
            <Paper className={classes.paper} elevation={0} >
                <Typography component="h2" variant="display1" gutterBottom>
                Habilidades
                </Typography>
                <Grid container spacing={24}>
                    {this.state.main}
                </Grid>
            </Paper>
        )
    }
}
export default withStyles(Styles)(Contact);