import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Paper, Grid, Avatar } from "@material-ui/core";
import ME from "../../me.jpg";
import Fade from "react-reveal/LightSpeed";

import Styles from "./styles";
class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      basic: {},
      git: {},
      main: ""
    };
  }

  componentDidMount = () =>
    this.setState(
      {
        basic: this.props.basic,
        git: this.props.git
      },
      () => this.buildData()
    );

  componentWillReceiveProps = ({ basic, git }) =>
    this.setState(
      {
        basic,
        git
      },
      () => this.buildData()
    );

  buildData = () => {
    const { basic, git } = this.state;
    const keys = Object.keys(basic);
    let main = keys.map(e => {
      if (basic[e].visible === true) {
        return (
          <Grid container spacing={8} alignItems="flex-end">
            <Grid item>
              <Fade left opposite cascade collapse>
                {basic[e].icon}
              </Fade>
            </Grid>
            <Grid item>
              <Typography
                variant={basic[e].variant}
                gutterBottom
                color="inherit"
              >
                {basic[e].label}
              </Typography>
            </Grid>
          </Grid>
        );
      }
    });
    this.setState({ main });
  };

  render() {
    const { classes } = this.props;
    const { basic, git } = this.state;
    return (
      <Paper className={classes.paper} elevation={0}>
        <Grid container spacing={24} justify="center" alignItems="center">
          <Grid item sm={4} lg={4} md={4}>
            <Grid container justify="center" alignItems="center">
              <Avatar alt="Remy Sharp" src={ME} className={classes.bigAvatar} />
            </Grid>
            <Grid container justify="center" alignItems="center">
              <span
                style={{
                  textAlign: "center",
                  fontSize: "11px",
                  fontWeight: "bold"
                }}
              >
                {basic.legend ? basic.legend.label : ""}
              </span>
            </Grid>
          </Grid>
          <Grid item sm={4} lg={4} md={4}>
            <div className={classes.margin}>{this.state.main}</div>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}
export default withStyles(Styles)(Contact);
