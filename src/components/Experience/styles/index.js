const Styles = theme => ({
    root: {
      flexGrow: 1,
      background: '#ccc'
    },
    paper: {
      padding: theme.spacing.unit * 2,
      color: "#fff",
    }, bigAvatar: {
      margin: 10,
      width: 150,
      height: 150,
  
    },
    avatar: {
      margin: 10,
      width: 50,
      height: 50,
    },
    icon: {
      margin: theme.spacing.unit * 2,
    },
    margin: {
      margin: theme.spacing.unit,
    },
    chip: {
      margin: theme.spacing.unit,
    },
  });

  export default Styles;