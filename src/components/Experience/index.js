import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Paper, Grid, Avatar } from "@material-ui/core";
import Styles from "./styles";
class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      experience: {},
      main: ""
    };
  }

  componentDidMount = () => {
    this.setState(
      {
        experience: this.props.experience
      },
      () => this.buildExperience()
    );
  };

  componentWillReceiveProps = ({ experience }) => {
    this.setState(
      {
        experience
      },
      () => this.buildExperience()
    );
  };

  buildExperience = () => {
    const { classes } = this.props;
    let main = this.state.experience.map(e => {
      return (
        <Grid container spacing={24}>
          <Grid item sm={12} lg={1} md={1}>
            <Grid container justify="center" alignItems="center">
              <Avatar
                alt="Remy Sharp"
                src={e.thumbnail}
                className={classes.avatar}
              />
            </Grid>
          </Grid>
          <Grid item sm={12} lg={11} md={11}>
            <div className={classes.margin}>
              <Grid container spacing={8}>
                <Grid item>
                  <Typography variant="body2" gutterBottom>
                    {`${e.name} | ${e.rol}`}
                  </Typography>
                  <Typography variant="caption" gutterBottom>
                    {`${e.time} `}
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    {`Actividades`}
                  </Typography>
                  <ul>
                    {e.activities.map(e => {
                      return (
                        <li>
                          <Typography variant="body2" gutterBottom>
                            {e}
                          </Typography>
                        </li>
                      );
                    })}
                  </ul>
                  <Typography variant="body2" gutterBottom>
                    {`Proyectos`}
                  </Typography>
                  <ul>
                    {e.projects.map(e => {
                      return (
                        <li>
                          <Typography variant="body2" gutterBottom>
                            {e}
                          </Typography>
                        </li>
                      );
                    })}
                  </ul>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
      );
    });
    this.setState({ main });
  };

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.paper} elevation={0}>
        <Typography component="h2" variant="display1" gutterBottom>
          Experiencia
        </Typography>
        {this.state.main}
      </Paper>
    );
  }
}
export default withStyles(Styles)(Contact);
