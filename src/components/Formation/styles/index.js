const Styles = theme => ({
    root: {
        flexGrow: 1,
        background: '#ccc'
    },
    paper: {
        padding: theme.spacing.unit * 2,
        color: "#fff",
    }, 
    avatar: {
        margin: 20,
        width: 50,
        height: 50,
      },
    icon: {
        margin: theme.spacing.unit * 2,
    },
    margin: {
        margin: theme.spacing.unit,
    },
    chip: {
        margin: theme.spacing.unit,
    },
});

export default Styles;
