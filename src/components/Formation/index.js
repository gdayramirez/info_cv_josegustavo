import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import { Typography, Paper, Grid,Avatar } from '@material-ui/core';
import Styles from './styles'

class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            school: {
            }
        }
    }

    componentDidMount = () => {
        this.setState({
            school: this.props.school
        })
    }

    componentWillReceiveProps = ({ school }) => {
        this.setState({
            school
        })
    }


    render() {
        const { classes } = this.props
        const { school } = this.state
        return (
            <Paper className={classes.paper} elevation={0} >
                <Typography component="h2" variant="display1" gutterBottom>
                    Formación
                </Typography>
                <Grid container spacing={24}>
                    <Grid item sm={12} lg={12} md={12}>
                        <div className={classes.margin}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Avatar alt="Remy Sharp" src={school.thumbnail} className={classes.avatar} />
                                </Grid>
                                <Grid item>
                                    <Typography variant="body2" gutterBottom>
                                        {school.name}
                                    </Typography>
                                    <Typography variant="body2" gutterBottom>
                                        {school.time}
                                    </Typography>
                                    <Typography variant="caption" gutterBottom>
                                        {school.title}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>
                    </Grid>
                </Grid>
            </Paper>
        )
    }
}
export default withStyles(Styles)(Contact);