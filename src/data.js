import React from "react";
import {
  AccountCircle,
  LocationOn,
  ContactPhone,
  Email,
  Computer
} from "@material-ui/icons";

const data = {
  languages: [
    {
      label: "English",
      percentage: 50
    }
  ],
  basic: {
    name: {
      variant: "body2",
      visible: true,
      icon: <AccountCircle style={{ paddingBottom: "10%" }} />,
      label: "José Gustavo Cruz Ramirez"
    },
    title: {
      variant: "body2",
      visible: true,
      icon: <Computer style={{ paddingBottom: "10%" }} />,
      label: "Senior React Developer"
    },
    address: {
      variant: "body2",
      visible: true,
      icon: <LocationOn style={{ paddingBottom: "10%" }} />,
      label: "Av. Thiers 145, Anzures, Ciudad de México"
    },
    phone: {
      variant: "body2",
      visible: true,
      icon: <ContactPhone style={{ paddingBottom: "10%" }} />,
      label: "5511446304"
    },
    email: {
      variant: "body2",
      visible: true,
      icon: <Email style={{ paddingBottom: "10%" }} />,
      label: "josegustavo.deve@outlook.com"
    },
    legend: {
      variant: "body2",
      visible: false,
      icon: "",
      label:
        "Tengo el propósito de ser uno de los mejores desarrolladores por lo que siempre doy el 100% en todo lo que hago. Yo no sólo me involucro en el desarrollo si no en todas las áreas que dependen de la mía y mi trabajo, soy colaborativo por lo tanto excelente trabajando en equipo"
    }
  },
  school: {
    name: "TESJo",
    title: "Ingeniería en sistemas computacionales",
    time: "Agosto 2012 - Enero 2017",
    thumbnail:
      "https://www.ipomex.org.mx/recursos/ipo/files_ipo3/img/0/6/b03adf3841e5ee29438338c22eee157c.jpg"
  },
  experience: [
    {
      time: "Agosto 2019 - Actualmente",
      name: "Interware de México",
      rol: "Senior React Developer",
      thumbnail:
        "https://media.licdn.com/dms/image/C4E0BAQH67YTHtHSphA/company-logo_200_200/0?e=2159024400&v=beta&t=ItYLfiq18pnXhwC-c1tKpd-IFGzo0fqednSSN_qGJoQ",
      activities: [
        "Desarrollar aplicaciones web utilizando React, Next.js, Redux, Node.js, Express, Webpack, Flow, Boostrap, Ant-Framework, Husky, Prettier.",
        "Maquetar aplicaciones web utilizando CSS3, Less, Flexbox, Grid Layout, HTML5.",
        "Utilizar Bamboo & Bitbucket para la integración continua de proyectos web"
      ],
      projects: [
        "Proyecto Bancario para evaluar documentación de posibles acreedores de crédito",
        "Proyecto bancario para facilitar el registro de donatarios y donativos",
        "Proyecto bancario para digitalizar carnet de clientes"
      ]
    },
    {
      time: "Julio 2018 - Julio 2019",
      name: "Magic Log S.A de C.V",
      rol: "Development Lead & Full Stack Developer",
      thumbnail:
        "https://media.licdn.com/dms/image/C560BAQGXzbQWgOfX5A/company-logo_200_200/0?e=2159024400&v=beta&t=l-udxuMFd-6DB2eVWHSv2FuMskInClQ59rtHn8gCAOg",
      activities: [
        "Desarrollar aplicaciones web con React.js, utilizando Redux, React Redux, Next.js, Material UI, CSS3, Sass, Webpack 4, Flow y servicios Azure.",
        "Desarrollar APIs Rest utilizando Node.js, Express, Postgresql y Microsoft Azure.",
        "Configurar App Service en Microsoft Azure para desplegar aplicaciones web.",
        "Desarrollar aplicaciones móviles utilizando React Native, React, CSS3, Flexbox, Expo Framework, Xcode.",
        "Ejecutar soluciones para desarrollo web y móvil, investigar nuevas tecnologías, liderar un equipo de desarrollo y asignar tareas para la colaboración en equipo.",
        "Colaborar con áreas como Bases de datos, Análisis, UI & UX, Administrativas, para mejorar el desempeño de cada proyecto."
      ],
      projects: [
        "Poseidon: Order Management System",
        "Sales Intelligence: Aplicación Web & Móvil para buscar clientes potenciales en WPS Database",
        "Lex Box: Aplicación web para crear contratos legales en línea."
      ]
    },

    {
      time: "Febrero 2017 - Agosto 2018",
      name: "MaxiPublica",
      rol: "Node.js Developer",
      thumbnail:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple113/v4/21/52/46/21524628-fe9e-4637-6d90-a187dd226395/source/60x60bb.jpg",
      activities: [
        "Desarrollar APIs Rest utilizando Node.js, Express, NPM, Mongo DB, Mongoose y servicios AWS.",
        "Desarrollar componentes web en Angular utilizando CSS3, HTML5 y Sass",
        "Desarrollar Webhooks con Node.js y servicios AWS",
        "Dar soporte a Servicios Web escritos en JAVA",
        "Conectar con servicios de Mercado Libre, Segunda mano, Vivanuncios, Facebook, Twitter, Soloautos, Autocosmos, para publicar anuncios de vehículos en venta",
        "Desarrollar componentes móviles utilizando Flexbox, CSS3, React, React Native"
      ],
      projects: [
        "Aplicación web que permite a las agencias de autos publicar su inventario desde un solo punto, así mismo poder tener mensajes, llamadas y métricas de sus anuncios, todo desde un solo sistema."
      ]
    }
  ],
  skils: [
    {
      name: "JavaScript",
      percentage: 90
    },
    {
      name: "React.js",
      percentage: 80
    },
    {
      name: "Node.js",
      percentage: 80
    },
    {
      name: "React Native",
      percentage: 60
    },
    {
      name: "Next.js",
      percentage: 80
    },
    {
      name: "Webpack 4",
      percentage: 80
    },
    {
      name: "Flow",
      percentage: 80
    },
    {
      name: "Jest Testing",
      percentage: 80
    },
    {
      name: "Enzyme Testing",
      percentage: 80
    },
    {
      name: "Axios",
      percentage: 100
    },
    {
      name: "Redux & React Redux",
      percentage: 80
    },
    {
      name: "React Hooks",
      percentage: 50
    },
    {
      name: "Express",
      percentage: 90
    },
    {
      name: "Mongoose",
      percentage: 80
    },
    {
      name: "Mongo DB",
      percentage: 60
    },
    {
      name: "AWS",
      percentage: 60
    },
    {
      name: "Azure",
      percentage: 60
    },
    {
      name: "Google Cloud Platform",
      percentage: 60
    },
    {
      name: "Docker",
      percentage: 60
    },
    {
      name: "NPM",
      percentage: 80
    },
    {
      name: "Sass",
      percentage: 70
    },
    {
      name: "CSS 3",
      percentage: 70
    },
    {
      name: "GIT",
      percentage: 80
    },
    {
      name: "SCRUM",
      percentage: 70
    },
    {
      name: "LINUX",
      percentage: 60
    }
  ]
};

export default data;
