const styles = theme => ({
    root: {
      flexGrow: 1,
      // background: '#fff',
      // paddingLeft : '20%',
      // paddingRight:'20%'
    },
    paper: {
      padding: theme.spacing.unit * 2,
      color: theme.palette.text.secondary,
    },
    bigAvatar: {
      margin: 10,
      width: 150,
      height: 150,
    },
    icon: {
      margin: theme.spacing.unit * 2,
    },
    margin: {
      margin: theme.spacing.unit,
    },
    chip: {
      margin: theme.spacing.unit,
    },
  });

  export default styles