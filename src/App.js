import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { MuiThemeProvider } from "@material-ui/core/styles";
import Data from "./data";
import Contact from "./components/Contact";
import Experience from "./components/Experience";
import Formation from "./components/Formation";
import Skills from "./components/Skills";
import Languages from "./components/Languages";
import Theme from "./Theme";
import Styles from "./Styles";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      height: "700px",
      basic: Data.basic,
      school: Data.school,
      experience: Data.experience,
      skils: Data.skils,
      git: Data.git,
      languages: Data.languages
    };
  }

  componentDidMount = () => {};
  componentWillMount = () =>
    this.setState({
      height: parseInt(window.innerHeight) + "px"
    });

  render() {
    const { classes } = this.props;
    const { basic, experience, school, skils, git, languages } = this.state;

    return (
      <MuiThemeProvider theme={Theme}>
        <div className={classes.root}>
          <Paper className={classes.paper} elevation={0}>
            <Grid container spacing={24}>
              <Grid item xs={12}>
                <Contact basic={basic} git={git}></Contact>
              </Grid>
              <Grid item sm={12} lg={8} md={8}>
                <Experience experience={experience}></Experience>
              </Grid>
              <Grid item sm={12} lg={4} md={4}>
                <Grid container spacing={24}>
                  <Grid item sm={12} lg={12} md={12}>
                    <Formation school={school}></Formation>
                  </Grid>
                  <Grid item sm={12} lg={12} md={12}>
                    <Languages languages={languages}></Languages>
                  </Grid>
                  <Grid item sm={12} lg={12} md={12}>
                    <Skills skils={skils}></Skills>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(Styles)(App);
